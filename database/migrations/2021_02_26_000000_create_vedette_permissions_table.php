<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVedettePermissionsTable extends Migration
{
    public function up()
    {
        Schema::create(
            "vedette_permissions", function (Blueprint $table) {
                $table->uuid("id")->primary();
                $table->string("name")->unique();
                $table->string("description");
                $table->string("slug")->nullable();
                $table->string("type")->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists("vedette_permissions");
    }
}
