<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVedettePermissionRoleTable extends Migration
{
    public function up()
    {
        Schema::create(
            "vedette_permission_role",
            function (Blueprint $table) {
                $table->uuid("id")->primary();
                $table->uuid("v_role_id");
                $table->uuid("v_permission_id");
                $table->unique(["v_role_id","v_permission_id"]);
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists("vedette_permission_role");
    }
}
