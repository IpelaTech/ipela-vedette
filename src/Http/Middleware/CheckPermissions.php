<?php
namespace IpelaVedette\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class CheckPermissions
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            
            auth()->user()->permissions()->each(
                function ($permission) {

                    if (!Gate::allows($permission["slug"])) {
                        abort(403);
                    }

                }
            );
        }

        return $next($request);
    }
}