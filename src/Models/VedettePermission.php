<?php
namespace IpelaVedette\Models;

use Illuminate\Support\Str;
use IpelaVedette\Models\Role;
use IpelaVedette\Models\VedetteRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class VedettePermission extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function booted()
    {
        static::created(
            function ($permission) {
                $permission->slug = Str::kebab($permission->name);
                $permission->save();
            }
        );
    }

    public function vedette_roles()
    {
        return $this->belongsToMany(VedetteRole::class, "vedette_permission_role", "v_role_id", "v_permission_id");
    }
}
