<?php

namespace IpelaVedette\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Str;
use IpelaVedette\Exceptions\VedetteException;
use IpelaVedette\Models\VedettePermission;
use IpelaVedette\Models\VedettePermissionRolePivot;
use IpelaVedette\Models\VedetteRole;

class VedetteRole extends Model
{
    use HasFactory;

    protected static function booted()
    {
        static::created(
            function ($role) {
                $role->slug = Str::slug($role->name, "-");
                $role->save();
            }
        );
    }

    protected $guarded = [];

    public function has_vedette_permission(VedettePermission $permission): bool
    {
        return $this->permissions->filter(
            function ($role_permission) use ($permission) {
                return $permission->id === $role_permission->id;
            }
        )
        ->count() > 0;
    }

    public function get_vedette_permissions()
    {
        return $this->permissions->map(
            function ($permission_pivot) {
                return [
                "slug" => $permission_pivot->get_permission_slug(),
                "name" => $permission_pivot->get_permission_name()
                ];
            }
        );
    }

    public function save_vedette_permissions(
        $permissions,
        string $resource = "",
        array $vedette_permission_types = []
    ) {
        foreach ($permissions as $permission) {
            if (!\is_subclass_of($permission, VedettePermission::class)
                && !\is_a($permission, VedettePermission::class)
            ) {
                throw new VedetteException("Item is not a VedettePermission, cannot add to {$this->name} permissions");
            }
        }

        if (trim($resource) === "" && count($vedette_permission_types) === 0) {
            $this->vedette_permissions()->saveMany($permissions);
            return;
        }
        $this->save_resource_permissions($resource, $vedette_permission_types);
    }

    public function save_resource_permissions(
        string $resource,
        array $vedette_permission_types = []
    ) {
        $permissions = VedettePermission::where("name", "LIKE", "%{$resource}%");

        foreach ($vedette_permission_types as $permission_type) {
            $permissions->where("name", "LIKE", "%{$permission_type}%");
        }

        $this->vedette_permissions()->saveMany($permissions->get());
    }

    public function has_permission(VedettePermission $permission)
    {
        return $this->vedette_permissions->filter(
            fn ($vedette_permission) =>
            $vedette_permission->id === $permission->id
        )->count() > 0;
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            "vedette_roles_users_pivot",
            "role_id",
            "user_id",
        );
    }

    public function vedette_permissions()
    {
        return $this->belongsToMany(
            VedettePermission::class,
            "vedette_permission_role",
            "v_permission_id",
            "v_role_id"
        )->withTimestamps();
    }
}
