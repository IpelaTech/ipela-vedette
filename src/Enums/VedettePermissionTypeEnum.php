<?php
namespace IpelaVedette\Enums;

use BenSampo\Enum\Enum;

final class VedettePermissionTypeEnum extends Enum
{
    const VIEWANY  = "ViewAny";
    const VIEW  = "View";
    const CREATE  = "Create";
    const UPDATE  = "Update";
    const DELETE  = "Delete";
}