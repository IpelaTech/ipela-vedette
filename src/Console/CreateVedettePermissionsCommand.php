<?php
namespace IpelaVedette\Console;

use Illuminate\Console\Command;
use IpelaVedette\Models\VedettePermission;
use IpelaVedette\Enums\VedettePermissionTypeEnum;


class CreateVedettePermissionsCommand extends Command
{
    protected $signature = "vedette:assign {resource}";

    protected $description = "Scaffold the required permissions resources";

    public function handle()
    {
        $resource = $this->argument("resource");
        
        collect(
            [
                VedettePermissionTypeEnum::VIEWANY(),
                VedettePermissionTypeEnum::VIEW(),
                VedettePermissionTypeEnum::CREATE(),
                VedettePermissionTypeEnum::UPDATE(),
                VedettePermissionTypeEnum::DELETE(),
            ]
        )->each(
            function ($permission_type) use ($resource) {
                VedettePermission::create(
                    [
                        "name" => "{$permission_type}{$resource}",
                        "description" => "Allow user to {$permission_type} {$resource}"
                    ]
                );
            }
        );

        $this->line("Permissions for {$resource} successfully created!");
    }
}
