<?php
namespace IpelaVedette\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use IpelaVedette\Console\CreateVedettePermissionsCommand;
use IpelaVedette\Console\PolicyMakeCommand;
use IpelaVedette\Http\Middleware\CheckPermissions;
use IpelaVedette\Models\Permission;

class IpelaVedetteBaseServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__."/../../database/migrations");
    }

    public function register()
    {
        // if ($this->app->runningInConsole()) {
        //     $this->commands(
        //         [
        //         CreateVedettePermissionsCommand::class,
        //         ]
        //     );
        // }
    }
}
