<?php

namespace IpelaVedette\Traits;

use IpelaVedette\Models\VedetteRole;
use IpelaVedette\Models\VedettePermission;

trait HasVedetteRole
{
    public function has_vedette_role(VedetteRole $role): bool
    {
        return null !== $this->roles->find($role->id);
    }

    public function has_vedette_roles(array $roles): bool
    {
        return $this->roles->whereIn("id", collect($roles)->pluck("id")->toArray())->count() > 0;
    }

    public function has_vedette_permissions(): bool
    {
        return count($this->get_vedette_permissions()) > 0;
    }

    public function get_vedette_permissions()
    {
        $vedette_role = $this->vedette_role()->first();

        if (null === $vedette_role) {
            return [];
        }

        return $vedette_role->vedette_permissions;
    }

    public function get_permission(VedettePermission $permission)
    {
        $vedette_permissions = $this->get_vedette_permissions();

        $permission = $vedette_permissions->filter(
            fn ($vedette_permission) => $vedette_permission->id === $permission->id
        );

        return $permission->count() > 0;
    }

    public function roles()
    {
        return $this->belongsToMany(
            VedetteRole::class,
            "vedette_roles_users_pivot",
            "user_id",
            "role_id",
        )->withTimestamps();
    }
}
