<?php
namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use IpelaVedette\Providers\IpelaVedetteBaseServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use RefreshDatabase;
    
    protected function defineEnvironment($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set(
            'database.connections.testbench',
            [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
            ]
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            IpelaVedetteBaseServiceProvider::class
        ];
    }
}
