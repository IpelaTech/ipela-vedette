<?php

use Tests\TestCase;
use Faker\Generator;
use Illuminate\Support\Str;
use Tests\mocks\VedetteUserMock;
use IpelaVedette\Models\VedetteRole;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use IpelaVedette\Models\VedettePermission;
use IpelaVedette\Exceptions\VedetteException;
use IpelaVedette\Enums\VedettePermissionTypeEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HasVedetteRoleTraitTest extends TestCase
{
    use RefreshDatabase;

    private $faker;
    private $user;
    private $role;
    
    public function setUp() : void
    {
        parent::setUp();
        $this->artisan("migrate:fresh");
        $this->faker = app(Generator::class);
    }

    public function create_user_and_role()
    {
        Schema::create(
            'vedette_user_mocks',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid("vedette_role_id")->nullable();
                $table->string('name');
                $table->string('email')->unique();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->string("type")->nullable();
                $table->rememberToken();
                $table->timestamps();
            }
        );

        $this->user = VedetteUserMock::create(
            [
                'name' => $this->faker->name(),
                'email' => $this->faker->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]
        );

        $this->role = VedetteRole::create(
            [
                "name" => "Test Role",
                "description" => "Test Description"
            ]
        );
    }

    public function test_can_get_role()
    {
        $this->create_user_and_role();
        //$this->role->users()->attach([$this->user->id]);
        $this->user->roles()->attach([$this->role->id]);
        $this->assertCount(1, $this->user->roles);
    }

    public function test_has_role_relationship()
    {
        $this->create_user_and_role();
        $this->user->roles()->attach([$this->role->id]);
        $non_role = VedetteRole::create(
            [
            "name" => "Non Role",
            "description" => "Non Role Description"
            ]
        );

        $this->assertTrue($this->user->has_vedette_role($this->role));
        $this->assertFalse($this->user->has_vedette_role($non_role));
    }

    public function test_associating_and_checking_permissions()
    {
        $this->create_user_and_role();
        $permission_one = VedettePermission::create(
            [
            "name" => "Permission One",
            "description" => "Permission One"
            ]
        );

        $permission_two = VedettePermission::create(
            [
            "name" => "Permission Two",
            "description" => "Permission Two"
            ]
        );

        $permission_three = VedettePermission::create(
            [
            "name" => "Permission Three",
            "description" => "Permission Three"
            ]
        );

        $permission_four = VedettePermission::create(
            [
            "name" => "Permission Four",
            "description" => "Permission Four"
            ]
        );
        $this->assertFalse($this->user->has_vedette_permissions());
        $this->assertCount(0, $this->user->get_vedette_permissions());
        
        $this->role->users()->save($this->user);
        $this->role->save_vedette_permissions([$permission_one, $permission_two]);

        $this->assertTrue($this->user->has_vedette_permissions());
        $this->assertCount(2, $this->user->get_vedette_permissions());
    }

    public function test_associate_permission_error()
    {
        $this->expectException(VedetteException::class);
        
        $this->create_user_and_role();
        $this->role->save_vedette_permissions([new VedetteRole]);
    }

    public function test_associate_resource_permissions()
    {
        $this->create_user_and_role();
        $this->artisan("vedette:assign Test1");
        $this->artisan("vedette:assign Test2");
        //$this->artisan("vedette:assign Test3");

        $permissions_one = VedettePermission::where("name", "LIKE", "%Test1%")
            ->get();
        $permissions_two = VedettePermission::where("name", "LIKE", "%Test2%")
            ->get();
        // $permissions_three = VedettePermission::where("name", "LIKE", "%Test3%")
        //     ->get();
        
        $this->assertFalse($this->user->has_vedette_permissions());
        $this->assertCount(0, $this->user->get_vedette_permissions());

        $this->role->save_vedette_permissions($permissions_one, "Test1");
        $this->assertTrue($this->user->has_vedette_permissions());
        $this->assertCount(5, $this->user->get_vedette_permissions());

        $this->role->save_vedette_permissions(
            $permissions_two,
            "Test2",
            [VedettePermissionTypeEnum::DELETE]
        );
        $this->assertCount(6, $this->user->get_vedette_permissions());
    }

    public function test_has_permissions()
    {
        $this->create_user_and_role();
        $this->artisan("vedette:assign Test1");
        $this->artisan("vedette:assign Test2");
        $permissions_one = VedettePermission::where("name", "LIKE", "%Test1%")
        ->get();
        $permissions_two = VedettePermission::where("name", "LIKE", "%Test2%")
        ->get();

        $this->role->save_vedette_permissions($permissions_one, "Test1");

        $this->assertTrue($this->role->has_permission($permissions_one->first()));
        $this->assertFalse($this->role->has_permission($permissions_two->first()));
    }

    public function test_can_get_permission()
    {
        $this->create_user_and_role();
        $this->artisan("vedette:assign Test1");
        $this->artisan("vedette:assign Test2");
        $permissions_one = VedettePermission::where("name", "LIKE", "%Test1%")
        ->get();
        $permissions_two = VedettePermission::where("name", "LIKE", "%Test2%")
        ->get();

        $this->role->save_vedette_permissions($permissions_one, "Test1");

        $permission = VedettePermission::where("name", "LIKE", "%ViewTest1%")->first();
        $this->assertTrue($this->user->get_permission($permission));
        $this->assertFalse($this->user->get_permission($permissions_two->first()));
    }
}
