<?php 

use Tests\TestCase;
use IpelaVedette\Enums\VedettePermissionTypeEnum;

class CreateVedettePermissionsCommandTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();
        $this->artisan("migrate:fresh");
    }

    public function test_can_create_permissions()
    {
        $model = "Test";
        $this->artisan("vedette:assign {$model}")
            ->expectsOutput("Permissions for {$model} successfully created!")
            ->assertExitCode(0);

        $this->assertDatabaseHas("vedette_permissions", ["name"=> VedettePermissionTypeEnum::VIEWANY().$model]);
        $this->assertDatabaseHas("vedette_permissions", ["name"=> VedettePermissionTypeEnum::VIEW().$model]);
        $this->assertDatabaseHas("vedette_permissions", ["name"=> VedettePermissionTypeEnum::CREATE().$model]);
        $this->assertDatabaseHas("vedette_permissions", ["name"=> VedettePermissionTypeEnum::UPDATE().$model]);
        $this->assertDatabaseHas("vedette_permissions", ["name"=> VedettePermissionTypeEnum::DELETE().$model]);
    }
}