<?php
namespace Tests\mocks;

use IpelaVedette\Models\VedetteRole;
use IpelaVedette\Traits\HasVedetteRole;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User;

class VedetteUserMock extends User
{
    use HasFactory, Notifiable, HasVedetteRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        "role_id",
        "type"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(
            VedetteRole::class,
            "vedette_roles_users_pivot",
            "role_id",
            "user_id",
        )->withTimestamps();
    }
}
